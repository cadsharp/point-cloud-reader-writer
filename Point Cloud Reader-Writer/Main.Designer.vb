﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdBrowseFilePath = New System.Windows.Forms.Button()
        Me.txtFilePath = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.optWrite = New System.Windows.Forms.RadioButton()
        Me.optRead = New System.Windows.Forms.RadioButton()
        Me.cmdProcess = New System.Windows.Forms.Button()
        Me.cmdAbout = New System.Windows.Forms.Button()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdBrowseFilePath)
        Me.GroupBox1.Controls.Add(Me.txtFilePath)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(450, 54)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Excel File"
        '
        'cmdBrowseFilePath
        '
        Me.cmdBrowseFilePath.Location = New System.Drawing.Point(410, 21)
        Me.cmdBrowseFilePath.Name = "cmdBrowseFilePath"
        Me.cmdBrowseFilePath.Size = New System.Drawing.Size(34, 19)
        Me.cmdBrowseFilePath.TabIndex = 1
        Me.cmdBrowseFilePath.Text = "..."
        Me.cmdBrowseFilePath.UseVisualStyleBackColor = True
        '
        'txtFilePath
        '
        Me.txtFilePath.Location = New System.Drawing.Point(10, 21)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.Size = New System.Drawing.Size(394, 20)
        Me.txtFilePath.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.optWrite)
        Me.GroupBox2.Controls.Add(Me.optRead)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 72)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(450, 70)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Process Mode"
        '
        'optWrite
        '
        Me.optWrite.AutoSize = True
        Me.optWrite.Location = New System.Drawing.Point(10, 42)
        Me.optWrite.Name = "optWrite"
        Me.optWrite.Size = New System.Drawing.Size(50, 17)
        Me.optWrite.TabIndex = 1
        Me.optWrite.TabStop = True
        Me.optWrite.Text = "Write"
        Me.optWrite.UseVisualStyleBackColor = True
        '
        'optRead
        '
        Me.optRead.AutoSize = True
        Me.optRead.Location = New System.Drawing.Point(10, 19)
        Me.optRead.Name = "optRead"
        Me.optRead.Size = New System.Drawing.Size(51, 17)
        Me.optRead.TabIndex = 0
        Me.optRead.TabStop = True
        Me.optRead.Text = "Read"
        Me.optRead.UseVisualStyleBackColor = True
        '
        'cmdProcess
        '
        Me.cmdProcess.Location = New System.Drawing.Point(12, 148)
        Me.cmdProcess.Name = "cmdProcess"
        Me.cmdProcess.Size = New System.Drawing.Size(98, 46)
        Me.cmdProcess.TabIndex = 2
        Me.cmdProcess.Text = "Process"
        Me.cmdProcess.UseVisualStyleBackColor = True
        '
        'cmdAbout
        '
        Me.cmdAbout.Location = New System.Drawing.Point(364, 148)
        Me.cmdAbout.Name = "cmdAbout"
        Me.cmdAbout.Size = New System.Drawing.Size(98, 46)
        Me.cmdAbout.TabIndex = 3
        Me.cmdAbout.Text = "About"
        Me.cmdAbout.UseVisualStyleBackColor = True
        '
        'ofd
        '
        Me.ofd.FileName = "OpenFileDialog1"
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(474, 206)
        Me.Controls.Add(Me.cmdAbout)
        Me.Controls.Add(Me.cmdProcess)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Main"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cmdBrowseFilePath As Button
    Friend WithEvents txtFilePath As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents optWrite As RadioButton
    Friend WithEvents optRead As RadioButton
    Friend WithEvents cmdProcess As Button
    Friend WithEvents cmdAbout As Button
    Friend WithEvents ofd As OpenFileDialog
End Class
