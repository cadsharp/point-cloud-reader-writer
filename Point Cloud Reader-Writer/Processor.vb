﻿Imports CADSharpTools
Imports CADSharpTools.CTModel
Imports SolidWorks.Interop.sldworks
Imports SolidWorks.Interop.swconst

Friend Class Processor
    Shared Sub Process(myInputs As Inputs)

        Try
            Select Case myInputs.ProcessMode
                Case Constants.ProcessModeEnum.Read
                    Read(myInputs)
                Case Constants.ProcessModeEnum.Write
                    Write(myInputs)
            End Select

            CTUtility.ShowMessage("Processing complete.", MessageBoxIcon.Information)
        Catch ex As Exception
            CTUtility.ShowMessage("Unhandled exception." + vbNewLine + ex.Message + vbNewLine +
                                  ex.StackTrace, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Shared Sub Read(myInputs As Inputs)
        'Get data from excel file
        Dim ex As Exception = Nothing
        Dim dt As DataTable = CTExcel.GetWorksheetData(myInputs.FilePath, "Sheet1", ex)
        If dt Is Nothing Then
            CTUtility.ShowMessage("Failed to get excel data." + vbNewLine + ex.Message)
            Exit Sub
        End If

        'Create SOLIDWORKS
        Dim app As SldWorks = CTStandAlone.CreateSolidWorks(True, 27)
        If app Is Nothing Then
            CTUtility.ShowMessage("Failed to create or get SOLIDWORKS.")
            Exit Sub
        End If

        'Create new part
        Dim model As ModelDoc2 = CTSolidWorks.CreateModel(app, swDocumentTypes_e.swDocPART)

        'Create a 3D sketch
        model.SketchManager.Insert3DSketch(True)

        'Add points
        model.SketchManager.AddToDB = True
        For Each dataRow As DataRow In dt.Rows
            model.SketchManager.CreatePoint(dataRow.ItemArray(0), dataRow.ItemArray(1), dataRow.ItemArray(2))
        Next
        model.SketchManager.AddToDB = False

        'Exit sketch
        model.SketchManager.Insert3DSketch(True)

        'Zoom to fit
        model.ViewZoomtofit2()
    End Sub

    Private Shared Sub Write(myInputs As Inputs)
        'Get SOLIDWORKS
        Dim app As SldWorks = CTStandAlone.CreateSolidWorks(True, 27)
        If app Is Nothing Then
            CTUtility.ShowMessage("Failed to create or get SOLIDWORKS.")
            Exit Sub
        End If

        'Verify part is open
        Dim model As ModelDoc2 = CTSolidWorks.GetActiveDocument(app, CTSolidWorks.CTDocTypes.Part)
        If model Is Nothing Then
            CTUtility.ShowMessage("No part active.")
            Return
        End If

        'Edit the first 3d sketch
        Dim features As List(Of CTFeature) = GetFeatures(model)
        Dim ctFeat As CTFeature = features.FirstOrDefault(Function(item) item.TypeName = "3DProfileFeature")
        If ctFeat Is Nothing Then
            CTUtility.ShowMessage("No 3D sketch found in this model.")
            Return
        End If

        'Get all sketch points
        Dim feat As Feature = ctFeat.Pointer
        Dim sketch As Sketch = feat.GetSpecificFeature2()
        Dim pts As Object = sketch.GetSketchPoints2()

        'Create empty table with correct number of rows and columns
        Dim dt As New DataTable()
        For i As Integer = 1 To 3
            dt.Columns.Add()
        Next

        Dim cells As String() = New String(2) {}
        For i As Integer = 0 To UBound(pts)
            dt.Rows.Add(cells)
        Next

        'Read data from each point into table
        For i As Integer = 0 To UBound(pts)
            Dim pt As SketchPoint = pts(i)
            dt.Rows(i)(0) = pt.X
            dt.Rows(i)(1) = pt.Y
            dt.Rows(i)(2) = pt.Z
        Next

        'Write to excel file
        Dim ex As Exception = Nothing
        If CTExcel.SetWorksheetData(myInputs.FilePath, 1, dt, ex) = False Then
            CTUtility.ShowMessage("Failed to write to Excel worksheet." + vbNewLine + ex.Message)
            Return
        End If
    End Sub

End Class
