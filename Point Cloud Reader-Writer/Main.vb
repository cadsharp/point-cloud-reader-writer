﻿Imports CADSharpTools
Imports System.IO

Public Class Main

    Private processMode As Constants.ProcessModeEnum

    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        LoadSettings()

        Text = CTUtility.GetAssemblyInfo(CTUtility.AssemblyInfo.ProductName)

        Icon = My.Resources.send
    End Sub

    Private Sub cmdProcess_Click(sender As Object, e As EventArgs) Handles cmdProcess.Click

        Dim myInputs As Inputs = VerifyInputs()
        If myInputs Is Nothing Then Exit Sub

        SaveSettings()

        cmdProcess.Enabled = False

        Processor.Process(myInputs)

        cmdProcess.Enabled = True
    End Sub

    Private Sub cmdAbout_Click(sender As Object, e As EventArgs) Handles cmdAbout.Click
        Dim myAboutForm As New CTAboutForm(Constants.AboutText)
        myAboutForm.ShowDialog()
    End Sub

    Private Function VerifyInputs() As Inputs
        Dim myInputs As New Inputs

        If File.Exists(txtFilePath.Text) Then
            myInputs.FilePath = txtFilePath.Text
        Else
            CTUtility.ShowMessage("Excel file does not exist.")
            Return Nothing
        End If

        myInputs.ProcessMode = processMode

        Return myInputs
    End Function

    Private Sub LoadSettings()
        txtFilePath.Text = My.Settings.FilePath
        processMode = My.Settings.ProcessMode

        Select Case processMode
            Case Constants.ProcessModeEnum.Read
                optRead.Checked = True
            Case Constants.ProcessModeEnum.Write
                optWrite.Checked = True
        End Select
    End Sub

    Private Sub SaveSettings()
        My.Settings.FilePath = txtFilePath.Text
        My.Settings.ProcessMode = processMode
        My.Settings.Save()
    End Sub

    Private Sub optRead_CheckedChanged(sender As Object, e As EventArgs) Handles optRead.CheckedChanged
        processMode = Constants.ProcessModeEnum.Read
    End Sub

    Private Sub optWrite_CheckedChanged(sender As Object, e As EventArgs) Handles optWrite.CheckedChanged
        processMode = Constants.ProcessModeEnum.Write
    End Sub

    Private Sub cmdBrowseFilePath_Click(sender As Object, e As EventArgs) Handles cmdBrowseFilePath.Click
        CTFormUtility.CreateBrowseFileDialog(txtFilePath, CTConstants.Filter_ExcelWorkbook, ofd)
    End Sub
End Class
