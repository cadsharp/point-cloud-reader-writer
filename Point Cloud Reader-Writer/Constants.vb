﻿Friend Class Constants
    Friend Enum ProcessModeEnum
        Read
        Write
    End Enum

    Friend Const AboutText As String = "This program has two modes: " + vbNewLine +
        "1. Read: Reads point cloud data from a user-specified Excel file and creates a 3DSketch in a new part." + vbNewLine +
        "2. Write: Reads point cloud data from the first 3D sketch in a part and writes the points to the user-specified Excel file." +
        "Contact keith@cadsharp.com for support."
End Class
